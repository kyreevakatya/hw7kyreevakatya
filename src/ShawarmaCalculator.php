<?php

declare(strict_types=1);

namespace Shawarma;

class ShawarmaCalculator
{
    private array $shawarmas = [];
    private array $ingredients = [];

    public function __construct()
    {
        $this->shawarmas = [];
        $this->ingredients = [];
    }

    public function add(Shawarma $shawarma)
    {
        $this->shawarmas[] = $shawarma;
        $this->ingredients[] = $shawarma->getIngredients();
    }

    public function price(): float
    {
        $price = 0.0;

        foreach ($this->shawarmas as $shawarma) {
            $price += $shawarma->getCost();
        }
        return $price;
    }

    public function ingredients(): array
    {
        return array_unique(array_merge(...$this->ingredients));
    }
}

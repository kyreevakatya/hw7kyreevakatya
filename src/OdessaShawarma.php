<?php

declare(strict_types=1);

namespace Shawarma;

final class OdessaShawarma extends Shawarma
{
    protected float $cost = 69.00;

    protected array $ingredients = ['огурцы маринованные', 'картофель жареный', 'чесночный соус', 'тандырный лаваш',
        'маринованный лук с барбарисом и зеленью', 'мясо куриное', 'салат коул слоу', 'корейская морковь'];

    protected string $title = 'Шаурма Одесская';
}

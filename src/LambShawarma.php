<?php

declare(strict_types=1);

namespace Shawarma;

final class LambShawarma extends Shawarma
{
    protected float $cost = 85.00;

    protected array $ingredients = ['острый соус', 'огурцы маринованные', 'кинза', 'помидоры свежие',
        'маринованный лук с барбарисом и зеленью', 'мясо баранины', 'лаваш арабский'];

    protected string $title = 'Шаурма из Баранины';
}

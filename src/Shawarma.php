<?php

declare(strict_types=1);

namespace Shawarma;

class Shawarma
{
    protected float $cost;
    protected array $ingredients;
    protected string $title;


    public function getCost(): float
    {
        return $this->cost;
    }


    /**
     * @return array
     */
    public function getIngredients(): array
    {
        return $this->ingredients;
    }


    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }
}

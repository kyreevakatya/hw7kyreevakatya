<?php

declare(strict_types=1);

require_once "../vendor/autoload.php";

$a = new Shawarma\OdessaShawarma();
$b = new Shawarma\BeefShawarma();
$c = new Shawarma\LambShawarma();

$h = new \Shawarma\ShawarmaCalculator();
$h->add($a);
$h->add($b);
$h->add($c);

var_dump($h->ingredients());
var_dump($h->price());
